require 'workarea/testing/teaspoon'

Teaspoon.configure do |config|
  config.root = Workarea::TrustpilotReviews::Engine.root
  Workarea::Teaspoon.apply(config)
end
