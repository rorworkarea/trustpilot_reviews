require 'test_helper'

module Workarea
  module Storefront
    class TrustpilotScriptSystemTest < Workarea::SystemTest
      include Storefront::SystemTest
      def test_trustpilot_script
        Workarea.with_config do |config|
          config.trustpilot_key = '2fBeAHvcRSrEVUcB'
          visit storefront.root_path

          assert page.has_xpath?('//div[@id="tp-snippet"]')

          config.trustpilot_key = nil
          visit storefront.root_path
          
          assert page.has_no_xpath?('//div[@id="tp-snippet"]')
        end
      end

      def test_trustbox_widget
        visit storefront.root_path
        assert(page.has_selector?('.trustpilot-widget'))
      end
    end
  end
end