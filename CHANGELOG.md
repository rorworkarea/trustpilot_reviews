#For Trustpilot Widget

  * Version 1.0.0.pre
    - Displays the trustpilot widget.

  * version 1.1.0
    - Display the trustpilot widget at the navigation by using "= append_ partials('storefront.page_header_trustpilot')" append partial.
    - Display the Carousel trustpilot widget by using "= append_ partials('storefront.trustpilot_carousel_widget')" append partial.
  * version 1.1.1
    - Updated the trustpilot widget for navigation.
