$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "workarea/trustpilot_reviews/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "workarea-trustpilot_reviews"
  spec.version     = Workarea::TrustpilotReviews::VERSION
  spec.authors     = ["Laxmi Pattar"]
  spec.email       = ["laxmi.p@trikatechnologies.com"]
  spec.homepage    = "http://workarea.com"
  spec.summary     = "Trustpilot is a review platform."
  spec.description = "Customers can review their experience about the companies through trustpilot."
  spec.license     = "Business Software License"

  spec.files = `git ls-files`.split("\n")

  spec.add_dependency 'workarea', '~> 3.x'
end
