Workarea::Plugin.append_partials(
  'storefront.document_head', 
  'workarea/storefront/trustpilot_script'
)

Workarea::Plugin.append_partials(
  'storefront.page-header__trustpilot', 
  'workarea/storefront/trustpilot_widget_at_nav'
)

Workarea::Plugin.append_partials(
  'storefront.trustpilot_carousel_widget', 
  'workarea/storefront/trustpilot_carousel_widget'
)
