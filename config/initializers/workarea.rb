Workarea.configure do |config|
  config.trustpilot_key = Rails.application.secrets.trustpilot_key
end
