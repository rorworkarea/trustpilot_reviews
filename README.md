Workarea Trustpilot Reviews
================================================================================

Customers can review their experience about the companies through trustpilot.

Overview
--------------------------------------------------------------------------------

1. TODO

Getting Started
--------------------------------------------------------------------------------

This gem contains a Rails engine that must be mounted onto a host Rails application.

* Then add the gem to your application's Gemfile specifying the source:

    # ...
    gem 'workarea-trustpilot_reviews'
    # ...

* Update your application's bundle.

    cd path/to/application
    bundle

* Use the following append partial
  #...
  = append_partials('storefront.page_header_trustpilot')

  For Carousel Type
  = append_ partials('storefront.trustpilot_carousel_widget')" append partial.
  #...

Features
--------------------------------------------------------------------------------

### TODO

Workarea Platform Documentation
--------------------------------------------------------------------------------

See [https://developer.workarea.com](https://developer.workarea.com) for Workarea platform documentation.

License
--------------------------------------------------------------------------------

Workarea Trustpilot Reviews is released under the [Business Software License](LICENSE)
