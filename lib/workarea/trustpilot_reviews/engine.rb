require 'workarea/trustpilot_reviews'

module Workarea
  module TrustpilotReviews
    class Engine < ::Rails::Engine
      include Workarea::Plugin
      isolate_namespace Workarea::TrustpilotReviews
    end
  end
end
